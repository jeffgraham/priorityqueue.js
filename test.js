var pqueue = require("./PriorityQueue.js");

function comp(first, second){
    return first < second;
}


function sorted(que){
    var previous = que.pop();
    while(que.size() > 0){
        var next = que.pop();
        if(comp(next, previous)){
            console.log(previous);
            return "Not sorted.";
        }
        console.log(next);
        previous = next;
    }
    return "Sorted!";
}

function randTest(n, max){
    var temp_queue = new pqueue(comp);
    for(var i = 0; i < n; i++){
        temp_queue.push(Math.ceil(Math.random()*max));
    }
    
    console.log(sorted(temp_queue));
    
}

randTest(4000, 1000);