/**
 * PriorityQueue structure for node.js
 *   based on the c++ stdlib interface definition
 * Parameters:
 * 1. comparator - a function object that takes
 *     two parameters, returns true if the first
 *     parameter is higher priority than the second
 *     otherwise returns false.
 */
function PriorityQueue(comparator){
    this.queue = [];
    this.comparator = comparator
}

/**
 * PriorityQueue.empty()
 *   Test whether container is empty.
 */
PriorityQueue.prototype.empty = function(){
    return this.queue.length === 0;
}

/**
 * PriorityQueue.size()
 *   Return size.
 */
PriorityQueue.prototype.size = function(){
    return this.queue.length;
}

/**
 * PriorityQueue.top()
 *   Access top element.
 */
PriorityQueue.prototype.top = function(){
    return this.queue[0];
}

/**
 * PriorityQueue.pop()
 *   Remove top element.
 */
PriorityQueue.prototype.pop = function(){
    //remove first element and put last element in its place
    swap(0, this.size()-1, this.queue);
    var value = this.queue.pop();
    
    //filter the new first element down into place
    var index = 0;
    var left = 0;
    var right = 0;
    var swapped = false;
    do{
        swapped = false;
        left = getLeft(index);
        right = getRight(index);
        if(right < this.size()){
            //find smaller element
            if(this.comparator(this.queue[left], this.queue[right])){
                if(this.comparator(this.queue[left], this.queue[index])){
                    //swap with left
                    swap(left, index, this.queue);
                    swapped = true;
                    index = left;
                }
            }
            else{
                if(this.comparator(this.queue[right], this.queue[index])){
                    //swap with right child
                    swap(right, index, this.queue);
                    swapped = true;
                    index = right;
                }
            }
            
        }
        else if(left < this.size()){
            //check left
            if(this.comparator(this.queue[left], this.queue[index])){
                //swap with left
                swap(left, index, this.queue);
            }
        }
    }while(swapped);
    
    return value;
}

/**
 * PriorityQueue.push(element)
 *   Inserts element.
 */
PriorityQueue.prototype.push = function(element){
    this.queue.push(element);
    var index = this.queue.length-1;
    var pIndex = getParent(index);
    while(pIndex >= 0 && this.comparator(this.queue[index], this.queue[pIndex])){
        //swap with parent
        swap(index, pIndex, this.queue);
        index = pIndex;
        pIndex = getParent(index);
    }
}

//helper functions
function swap(index1, index2, array){
    var temp = array[index1];
    array[index1] = array[index2];
    array[index2] = temp;
}
function getParent(index){
    return (index + (index % 2 - 1) - 1) / 2;
}
function getLeft(pIndex){
    return 2 * pIndex + 1;
}
function getRight(pIndex){
    return 2 * pIndex + 2;
}

module.exports = PriorityQueue;